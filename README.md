# FPV Bot 2.0 - Altium Files

This is the most recent DC motor control board. It features:

* 32-bit arm controller with m4 DSP
* 2x Motor Drivers
* Battery Monitor
* Low Battery Buzzer
* All power routing (5V & 12V Switching Regulators)
* Reverse Polarity Protection

## Schematic

![Servo Controller Schematic](schematic.png)  

## PCB Renderings

![PCB Top](pcb-top.png)

![PCB Bottom](pcb-bottom.png)

## Board Functions

This board perform all timing critical operations for the robot, namely

* Reading quadrature encoder inputs

* FIR filtering of motor speed (very noisy input as encoder counts are a quantized input)

* PID control of motor position

* Over current protection of DC motors

* Over temperature protection of DC motors

  

## Robot Motion Operation

The firmware from the board is updatable from anywhere in the world (updating from Seychelles to UCSB).

* A remote desktop runs PlatformIO & MBed to compile and build any firmware changes. 
* These compiled files are pushed to the Jetson Nano board via RSync.
* The Jetson Nano uploads the new code to the 32 bit controller via OpenOCD & GDB

During runtime, the robot accepts motion commands:

* A task manager program (written in Python & NodeJS) verifies that all essential scripts are up and running.
* A TCP socket reads and verifies all incoming motion coordinates
* A python script converts these changes in robot position to motor encoder counts
* Changes in motor position is transmitted over serial to 32-bit arm controller



# Remote Servo Tuning

## Click the picture below to see the YouTube video.

[![Remote Motor Tuning](http://img.youtube.com/vi/m8hOdliGSg8/0.jpg)](https://youtu.be/m8hOdliGSg8 "Remote Motor Tuning")

Here is a brief video showing how remote firmware updates and motor tuning work. 

The tool chain is the same as above, only the network socket has been setup to to accept PID tuning parameters that can be written to from a python script running on the desktop.

Calibration begins at 2:20.